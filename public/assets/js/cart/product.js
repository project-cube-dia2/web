function updateTotalPrice(){
    /* let description = document.querySelectorAll('#description-prix > p');
    let total = 0;
    description.forEach(element => {
        total += parseInt(element.innerHTML.replace(/€/g, ''));
    });
    console.log(total); */
    
    let totalPrice = 0;
    let product = document.querySelectorAll('#product');

    let productsCount = 0;

    product.forEach(element => {
        let prix = element.querySelector('#cart-prix').innerHTML;
        let quantite = element.querySelector('#cart-quantite').innerHTML;
        productsCount += parseInt(quantite);
        totalPrice += parseInt(prix.replace(/€/g, '')) * parseInt(quantite);
    });
    
    // productsCount = product.length;
    let displayPrice = document.querySelector('#total-price-card');

    displayPrice.innerHTML = `Sous-total (${productsCount} articles): ${totalPrice}€`;
}

function addToQuantitePanier(element){
    let quantite = element.explicitOriginalTarget.parentNode.querySelector('span');
    let value = quantite.innerHTML;
    value = parseInt(value) +1;
    quantite.innerHTML = value;
    
    updateTotalPrice();
}

function removeToQuantitePanier(element){

    let product = element.explicitOriginalTarget.parentNode.closest("#product");
    let grid = product.parentNode;

    let quantite = element.explicitOriginalTarget.parentNode.querySelector('span');
    let value = quantite.innerHTML;
    value = parseInt(value) - 1;
    
    if(value > 0){
        quantite.innerHTML = value;
    }else{
        grid.removeChild(product);
    }
    
    updateTotalPrice();
}

// Ajout d'un article au panier depuis la page panier
let btnPlus = document.querySelectorAll("#btn-plus");
btnPlus.forEach(btn => {
    btn.addEventListener('click',async function(e){
        
        //Demande au serveur d'ajouter l'article au panier sauvegardé dans la session utilisateur
        await fetch("http://negosud2/panier/add", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: `id=${e.target.value}&quantite=${1}`
        })
        .then(function(response) {
            // console.log(response.json());
            return response.json();
        })
        .then(function(json) {
            // console.log(json);
            return json;
        });

        /**
         * Fonction de banner.js
         * Met à jour le compteur d'article dans la bannière
         */
        updateBannerCartCount();

        /**
         * Mettre à jour le compteur d'article dans la page panier
         */
        addToQuantitePanier(e);
    });
});

// Retirer article au panier depuis la page panier
let btnMoins = document.querySelectorAll("#btn-moins");
btnMoins.forEach(btn => {
    btn.addEventListener('click',async function(e){
        
        //Demande au serveur d'ajouter l'article au panier sauvegardé dans la session utilisateur
        await fetch("http://negosud2/panier/remove", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: `id=${e.target.value}&quantite=${1}`
        })
        .then(function(response) {
            // console.log(response.json());
            // return response.json();
        })
        .then(function(json) {
            // console.log(json);
            // return json;
        });

        /**
         * Fonction de banner.js
         * Met à jour le compteur d'article dans la bannière
         */
        updateBannerCartCount();

        /**
         * Mettre à jour le compteur d'article dans la page panier
         */
        removeToQuantitePanier(e);
    });
});