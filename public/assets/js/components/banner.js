
/**
 * Retourne liste des /web/produits dans le panier
 * @returns Array
 */
async function getCart(){
    return await fetch("http://negosud2/panier/get", {
        method: "GET",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
        })
        .then(async function(response) {
            return await response.json();
        })
        .then(async function(json) {
            return await json;
        });
}

/**
 * Actualise le compteur d'article de la bannière
 */
async function updateBannerCartCount(){
    let bannerCart = await getCart();
    let panier = document.getElementById('panier');

    console.log(bannerCart.length)
    panier.innerHTML = `Panier (${bannerCart.length})`;
}

updateBannerCartCount();
