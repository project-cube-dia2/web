
/* console.log(products);
console.log(filters); */

//Ajout des maisons aux filtres
let maisonFilters = document.getElementById("famille");
filters.famille.forEach(family => {
    let option = document.createElement("option");
    if(family === filters.userSettings.famille){
        option.selected = true;
    }
    option.setAttribute('value', family);
    option.innerText = family;
    maisonFilters.appendChild(option);
});

//Ajout des types de vin aux filtres
let typeFilters = document.getElementById("type");
filters.type.forEach(type => {
    let option = document.createElement("option");
    if(type === filters.userSettings.type){
        option.selected = true;
    }
    option.setAttribute('value', type);
    option.innerText = type;
    typeFilters.appendChild(option);
});

//Ajout des millésimes aux filtres
let yearsFilters = document.getElementById("annee");
filters.annee.forEach(year => {
    let option = document.createElement("option");
    if(year == filters.userSettings.annee){
        option.selected = true;
    }
    option.setAttribute('value', year);
    option.innerText = year;
    yearsFilters.appendChild(option);
});

//Ajout des prixVente aux filtres
let priceFilters = document.getElementById("prixVente");
let priceBrackets = document.querySelector('input[type="range"]');
let priceDisplay = document.getElementById("price-display");

// let value = priceBrackets.value;
let value = filters.userSettings.prixVente;
priceBrackets.value = value;
priceDisplay.innerText = `jusqu'à ${Math.floor((filters.prixVente.max * value) / 100)}€`;

priceBrackets.addEventListener('change', function() {
    let value = priceBrackets.value;
    priceDisplay.innerText = `jusqu'à ${Math.floor((filters.prixVente.max * value) / 100)}€`;
  });


// Ajout d'un article au panier depuis le catalogue
let carts = document.querySelectorAll(".cart");
carts.forEach(cart => {
    cart.addEventListener('click',async function(e){
        let footerCard = e.target.parentNode;
        let card = footerCard.parentNode;
        let cardCopy = card.cloneNode(true);
        cardCopy.id = "copy";

        let btnCart = cardCopy.querySelector('.card-footer');
        let footer = btnCart.querySelector('button');
        btnCart.removeChild(footer);

        let modalBody = document.querySelector(".modal-body");  
        modalBody.appendChild(cardCopy);
        
        //Demande au serveur d'ajouter l'article au panier sauvegardé dans la session utilisateur
        await fetch("http://negosud2/panier/add", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: `id=${e.target.value}&quantite=${1}`
        })
        .then(function(response) {
            // console.log(response.json());
            return response.json();
        })
        .then(function(json) {
            // console.log(json);
            return json;
        });

         // Fonction de banner.js
         // Met à jour le compteur d'article dans la bannière
         //
        updateBannerCartCount();
    });
});


// Ajout d'un article au panier depuis la page panier
let btnPlus = document.querySelectorAll("#btn-plus");
btnPlus.forEach(btn => {
    btn.addEventListener('click',async function(e){
        /* //Demande au serveur d'ajouter l'article au panier sauvegardé dans la session utilisateur
        await fetch("http://negosud2/panier/add", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: `id=${e.target.value}&quantite=${1}`
        })
        .then(function(response) {
            // console.log(response.json());
            return response.json();
        })
        .then(function(json) {
            console.log(json);
            return json;
        }); */

        /**
         * Fonction de banner.js
         * Met à jour le compteur d'article dans la bannière
         */
        // updateBannerCartCount();
    });
});



//Efface la contenu de la modale à la fermeture
let modal = document.getElementById("cartModal");
modal.addEventListener('hide.bs.modal', () => {
    let modalBody = document.querySelector(".modal-body");
    let copy = document.getElementById("copy");
    modalBody.removeChild(copy);
  })
