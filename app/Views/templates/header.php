<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= esc($title) ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <?php
        if(isset($css) && $css != ''){
            foreach($css as $file){
                echo "<link href=" . base_url() . "/negosud/public/assets/css/$file rel='stylesheet'>";
            }
        }
    ?>
    <!-- <link href="http://localhost/negosud/public/assets/css/test/test.css" rel="stylesheet"> -->
    
</head>
<body class="p-0 m-0 d-flex justify-content-center align-items-center row">
