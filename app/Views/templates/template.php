<!doctype html>
<html>
<head>
    <title><?= esc($title) ?></title>
</head>
<body>

    <h1><?= esc($title) ?></h1>
    
    <?= $banner ?>

    <em>&copy; 2021</em>
</body>
</html>