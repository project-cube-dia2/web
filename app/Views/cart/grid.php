<div id="grid" class="container-fluid d-flex justify-content-center align-items-center flex-column">
    <?php

        $totalPrice = 0;
        if(! empty($cart) && is_array($cart)){

            foreach($cart as $product){
                if($product['prixVente'] != null){
                    $totalPrice += ((int)$product['prixVente'] * $product['quantite']);
                }
                include('product.php');
            }
        }
    ?>
        <div id="total-price" class="card" >
            <div class="card-body">
                <p id="total-price-card" class="card-text">Sous-total (<?= count($cart) ?> articles): <?= $totalPrice ?>€</p>
            </div>
        </div>
    <div class="d-flex fluid-container">
        <button class="btn btn-danger">Annuler</button>
        <!-- <button type="submit" class="btn btn-success">Acheter</button> -->
        <button class="btn btn-success cart d-flex justify-content-center align-items-center" value="" data-bs-toggle="modal" data-bs-target="#cartModal" type="button">
            Valider
        </button>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="cartModal"  tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" method="post">

        <form action="" method="post">
            <div class="modal-header">
              <h1 class="modal-title fs-5" id="exampleModalLabel">Confirmez votre commande</h1>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
      
            <div class="modal-body">
                <!-- Carte information client --> 
                <div class="card">
                    <div class="card-header">
                        Informations client
                    </div>

                    <div class="card-body">
                        <div class="input-group mb-3">
                            <!-- <label for="nom" class="form-label">Nom</label> -->
                            <span class="input-group-text col-2">Prénom</span>
                            <input type="text" name="prenom" class="form-control" placeholder="votre prénom" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text col-2">Nom</span>
                            <input type="text" name="nom" class="form-control" placeholder="votre nom" required>
                        </div>
                        
                        <div class="input-group mb-3">
                            <span class="input-group-text col-2">Email</span>
                            <input type="email" name="email" class="form-control" placeholder="name@example.com" required>
                        </div>
                    </div>

                    
                </div>

                <!-- Carte adresse de livraison -->
                <div class="card">
                    <div class="card-header">
                        Informations de livraison
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <span class="input-group-text col-2">Pays</span>
                            <input type="text" name="pays" class="form-control" placeholder="pays" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text col-2">Ville</span>
                            <input type="text" name="ville" class="form-control" placeholder="adresse de livraison" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text">Rue</span>
                            <input type="text" name="rue" class="form-control" placeholder="rue et numéro de rue" required>
                        </div>
                    </div>
                    
                </div>
            </div>
      
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Continuer mes achats</button>
              <input id="btn-valider" type="submit" value="Confirmer ma commande" class="btn btn-success"></input>
            </div>
        </form>

    </div>
  </div>
</div>
