
  <div id="product" class="card mb-3">
        <div class="row g-0">
            <div class="d-flex justify-content-center align-items-center col-md-2">
                <!-- <img src="..." class="img-fluid rounded-start" alt="..."> -->
                <img id="photo-produit" class="img-fluid rounded-start" alt="photo produit" src=<?= base_url() . "/negosud/public/assets/images/bottle.jpg" ?> class="card-img-top" alt="">
            </div>
            <div class="col-md-6">
                <div id="description-produit" class="card-body p-0">
                    <h5 class="card-title card-header text-center"><?= $product['nomArticle']?></h5>
                    <div>
                        <p class="card-text text-center"><?= $product['famille'] . " - " . $product['type'] . " - " . $product['annee'] ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div id="description-prix" class="card-body p-0">
                    <h5 class="card-title card-header text-center">Prix</h5>
                    <p id="cart-prix" class="card-text text-center"><?= $product['prixVente']?>€</p>
                </div>
            </div>
            <div class="col-md-2">
                <div id="description-produit" class="card-body p-0 ">
                    <h5 class="card-title card-header text-center">Quantite</h5>
                    <div class="fluid-container align-items-center">
                        <div id="cart-edit" class="card-text text-center gestion-panier">
                            <button id="btn-plus" value="<?= $product['id']; ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                </svg>
                            </button>
                            <span id="cart-quantite"><?=  $product['quantite'] ?></span>
                            <button id="btn-moins" value="<?= $product['id']; ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-dash-circle" viewBox="0 0 16 16">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                </svg>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>
