<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet"href="acceuil.css"/>
  </head>
  <body>
    <header class="cave" id="home">
    </header>
    <div class="test_2">
        <div class="boxa">
          <img src="<?= base_url()?>/negosud/public/assets/images/Degustation.jpg" style="width: 100%; height: 500px;padding:0px; margin:0px;" />
        </div>
        <div class="boxb">
          <p>
            
            Introduction à la dégustation<br><br>
            La dégustation de vin en un sens, c’est le fait de consommer le vin. <br>
            Ce n’est pas uniquement dans le but de goûter au vin. Effectivement, <br>
            la dégustation du vin met en exergue l’utilisation de trois sens pour <br>
            profiter pleinement de ce vin. Ces sens sont successivement la vue, <br>
            l’odorat et le goût.<br>
            Dans un premier temps, il y a le plaisir visuel. En effet, la <br>
            dégustation visuelle se fait par le biais d’une analyse de la texture <br>
            du vin par le biais d’une attention soutenue du regard. Ainsi, il est <br>
            possible de distinguer la texture du vin mais également sa couleur.<br>
            Dans un second temps, le plaisir olfactif est de mise. Assurément, <br>
            l’odeur joue un rôle essentiel en ce qui concerne la qualité du vin.<br>
            Cela fait partie des caractéristiques propres à ce vin. Il ne faut pas <br>
            négliger ce détail qui pourrait faire la différence en ce qui concerne <br>
            l’unicité de ce vin.<br>
            <br>
          </p>
        </div>
      </div>
      <div class="test_3">
        
        <img src="<?= base_url()?>/negosud/public/assets/images/multiplication-sens.jpg" style="width: 100%; height: 500px; padding:0px; margin:0px;" />
        <div class="texte3" style="text-align:center;font-size: large;font-family: 'Inter';">
          <p>
            
            Dégustation à l’aveugle<br><br>
            Vous souhaitez divertir vos invités avec une dégustation de vin à <br>
            l’aveugle. Il y a quelques étapes à prendre en compte pour que chacun <br>
            puisse profiter de ce moment de façon optimale. C’est une aventure à <br>
            la fois amusante et stimulante pour peu que l’organisation soit au top.<br>
            Chacun doit partir sur un pied d’égalité pour profiter de ce moment.<br>
            Tout d’abord, il est important de biaiser la vue des concurrents. <br>
            La robe du vin pourrait en mettre certains sur la voie. Il est conseillé <br>
            de faire usage des verres noirs pour camoufler la robe du vin. Ainsi, <br>
            les invités ne peuvent se fier à leur vue pour la dégustation, cela <br>
            reste une véritable surprise.<br>
            Pour ce qui est de la sélection de vin que vous allez présenter durant <br>
            votre petite fête, deux options s’offrent à vous. En premier, vous pouvez<br>
            opter pour une sélection ouverte. Dans cette optique, tous types de vin <br>
            sont admis. La seconde option est axée sur des thèmes spécifiques. En <br>
            d’autres termes, seuls les vins qui respectent le thème sont admis<br><br><br>
          </p>
        </div>
      </div>

  </body>
</html>
