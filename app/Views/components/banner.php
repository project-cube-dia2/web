
<nav id="banner" class="m-0 p-0 navbar navbar-expand-lg bg-body-tertiary">
  <a class="m-0 p-0 navbar-brand" href="#">
      <?php
          echo "<img src=" . base_url() . '/negosud/public/assets/images/logo.png alt="Logo" width="50px" height="50px">';
      ?>
  </a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="container-fluid">

    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
      <ul class="navbar-nav">
        
        <li class="nav-item">
          <a class="nav-link" href="<?= "/pages/accueil" ?>">Accueil</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?= "/catalogue" ?>">Cave</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="pages/degustation">Dégustation</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="pages/propos">A propos</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="pages/contact">Contact</a>
        </li>

        <li class="nav-item">
          <a id="panier" class="nav-link" href="<?= "/panier" ?>">Panier </a>
        </li>

      </ul>
    </div>
  </div>
</nav>
