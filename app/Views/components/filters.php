
<form id="filtres" class="d-flex flex-row justify-content-evenly align-items-center bg-body-tertiary py-2" method="post">
  <div class="mb-3 d-flex flex-column align-items-center">
    <label for="famille" class="form-label">Maison</label>
    <select name="famille" id="famille" class="form-select" aria-label="Maison">
    </select>
  </div>

  <div class="mb-3 d-flex flex-column align-items-center">
    <label for="type" class="form-label">Famille de vin</label>
    <select name="type" id="type" class="form-select" aria-label="Default select example">
    </select>
  </div>

  <div class="mb-3 d-flex flex-column align-items-center">
      <label for="annee" class="form-label" >Millésime</label>
      <select name="annee" id="annee" class="form-select" aria-label="Default select example">
    </select>
  </div>

  <div class="mb-3 d-flex flex-column align-items-center">
    <label for="prixVente" class="form-label">Prix</label>
    <input name="prixVente" id="prixVente" type="range" class="form-range">
    <div id="price-display"></div>
  </div>


  
  <button type="submit" class="btn btn-lg btn-primary">Valider</button>
</form>
