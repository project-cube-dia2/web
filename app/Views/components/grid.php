<div id="grid" class="row row-cols-1 row-cols-md-4 g-4">
    
<?php

    if(! empty($products) && is_array($products)){
        
        foreach($products as $product){
            include('product.php');
        }
        
    }

?>

</div>

<!-- Modal -->
<div class="modal fade" id="cartModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Ajouté au panier !</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Continuer mes achats</button>
        <button id="btn-valider" onclick="window.location.href='panier';" type="button" class="btn btn-primary">Voir mon panier</button>
      </div>
    </div>
  </div>
</div>

