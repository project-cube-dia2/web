


<div id="product" class="col">
    <div class="card m-0 p-0 d-flex justify-content-center align-items-center">
        <div class="card-header d-flex justify-content-center align-items-center w-100">
            <h5 class="card-title text-center"><?= $product['nomArticle'] ?></h5>
        </div>

        
        <div class="card-body p-2 w-75 d-flex justify-content-center align-items-center row">
            <img src=<?= base_url() . "/negosud/public/assets/images/bottle.jpg" ?> class="card-img-top" alt="">
            <p id="product-description" class="card-text text-center">
                <?= $product['famille'] ?> - <?= $product['type'] ?> - <?= $product['annee'] ?>
            </p>
        </div>
        
        <div class="card-footer p-2 w-100 d-flex align-items-center">
            <div class="col text-muted text-center"><?= $product['prixVente'] ?>€</div>

            <button class="btn btn-primary cart d-flex justify-content-center align-items-center" value="<?= $product['idArticle']?>" data-bs-toggle="modal" data-bs-target="#cartModal" type="button"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
  <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
</svg></button>
        </div>
    </div>
  </div>
