<?php

namespace App\Models;

use CodeIgniter\Model;

class OrdersModel extends Model
{
    private $client;

    function __construct()
    {
        /* $this->client = \Config\Services::curlrequest([
            'baseURI' => 'http://localhost:8080/apiv2/',
        ]); */
        $this->client = \Config\Services::curlrequest([
            'baseURI' => 'https://cube.fertille.fr/api/',
        ]);
    }

    /**
     * Créer une nouvelle commande en BDD
     * Retourne l'id en cas de succès, sinon retourne null
     */
    public function createOrder($customer, $cart){

        //Calcul du coût total du panier
        $cartPrice = 0;
        foreach($cart as $product){
            $cartPrice += ($product['prixVente'] * $product['quantite']);
        }
        
        //On récupère l'id le plus élevé de la table commande pour calculer l'id à donner à la nouvelle commande
        $newOrderId = ($this->getMaxID() + 1);

        //On fixe la date de livraison à j+10 par défaut
        $date = date("Y-m-d", time() + (10 * 24 * 60 * 60));
        
        $body = [
            'adresseLivraison' => (string)$customer['adresse'],
            'client' => (string)$customer['id'],
            'cout' => (string)$cartPrice,
            'dateReception' => (string)$date,
            'idCommande' => (string)$newOrderId
        ];

        $response = $this->client->request('POST', "commandes",
        ['json'=>$body,
        'headers' => ['Access-Control-Allow-Origin' => '*']
        ]);

        $codeStatus = $response->getStatusCode();

        if($codeStatus == 200 || $codeStatus == 204){
            return $newOrderId;
        } else {
            return null;
        }
    }

    private function getMaxID(){
        $orders = $this->getAllOrders();
        $maxID = 0;

        foreach($orders as $order){
            $maxID = $maxID > $order['idCommande'] ? $maxID : $order['idCommande'];
        }

        return (int)$maxID;
    }
    
    public function getOrderById($id){
        $response = $this->client->request('GET', "commandes/$id");
        $body = json_decode($response->getBody(), true);
        
        return $body;
    }
    
    public function getAllOrders(){
        $response = $this->client->request('GET', 'commandes');
        $body = json_decode($response->getBody(), true);

        return $body['commande'];
    }
    
}