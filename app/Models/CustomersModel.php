<?php

namespace App\Models;

use CodeIgniter\Model;

class CustomersModel extends Model
{
    private $client;

    function __construct()
    {
        /* $this->client = \Config\Services::curlrequest([
            'baseURI' => 'http://localhost:8080/apiv2/',
        ]); */
        $this->client = \Config\Services::curlrequest([
            'baseURI' => 'https://cube.fertille.fr/api/',
        ]);
    }

    public function existsInDB($arr){

        // var_dump($arr);
        
        if(isset($arr['prenom']) && isset($arr['nom'])){
            // echo "PRENOM and NOM are set<br><br>";
            $customers = $this->getAllCustomers();
            
            foreach($customers as $customer){
                if((string)strtoupper($customer['nom']) == (string)strtoupper($arr['nom']) && (string)strtoupper($customer['prenom']) == (string)strtoupper($arr['prenom'])){
                    return (int)$customer['id'];
                }
            }
        }

        return false;
    }
    
    public function createCustomer($arr){

        $body = [
            'adresse' => strtoupper($arr['pays']) . ",". ucfirst(strtolower($arr['ville'])) . ",". $arr['rue'],
            'email' => $arr['email'],
            'mdp' => 'QUfcJOJbtbkPu1T',
            'nom' => ucfirst(strtolower($arr['nom'])),
            'prenom' => ucfirst(strtolower($arr['prenom'])),
        ];
        
        $response = $this->client->request('POST', "clients", ['json'=>$body]);
        $codeStatus = $response->getStatusCode();

        return $codeStatus;
    }
    
    public function getCustomersById($id){
        $response = $this->client->request('GET', "clients/$id");
        $body = json_decode($response->getBody(), true);
        
        return $body;
    }
    
    public function getAllCustomers(){
        $response = $this->client->request('GET', 'clients');
        $body = json_decode($response->getBody(), true);

        return $body['clients'];
    }
    
}