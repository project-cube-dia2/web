<?php

namespace App\Models;

use CodeIgniter\Model;

class ProductsModel extends Model
{
    private $client;

    function __construct()
    {
        /* $this->client = \Config\Services::curlrequest([
            'baseURI' => 'http://localhost:8080/apiv2/',
        ]); */
        $this->client = \Config\Services::curlrequest([
            'baseURI' => 'https://cube.fertille.fr/api/',
        ]);
    }
    
    public function getProducts($params = false)
    {
        if($params){
            return $this->getProductsByFilters($params);
        }
        else{
            return $this->getAllProducts();
        }

    }

    private function getAllProducts(){
        $response = $this->client->request('GET', 'web/produits');
        $body = json_decode($response->getBody(), true);

        return $body['produit'];
    }

    public function getProductById($id){
        $response = $this->client->request('GET', "web/produits/$id");
        $body = json_decode($response->getBody(), true);
        
        return $body;
    }

    private function getProductsByFilters($filters)
    {
        $products = $this->getAllProducts();
        $maxPrice = $this->getMaxPrice($products);

        foreach($products as $productKey => $product){
            foreach($filters as $filterKey => $filterValue){
                
                if(isset($product[$filterKey])){
                    switch($filterKey){
                        case 'famille':
                        case 'type':
                            if($filterValue != 'Tout' && $product[$filterKey] != $filterValue){
                                unset($products[$productKey]);
                            }
                            break;
                        case 'annee':
                            if($filterValue != 'Tout' && ((int)$product[$filterKey] < (int)$filterValue || (int)$product[$filterKey] > ((int)$filterValue+9))){
                                unset($products[$productKey]);
                            }
                            break;
                        case 'prixVente':
                            if(((int)$product['prixVente'] > (($maxPrice * (int)$filterValue)) / 100)){
                                unset($products[$productKey]);
                            }
                            break;
                    }
                }
            }
        }

        return $products;
        
    }

    public function getProductFilters(){
        $response = $this->client->request('GET', 'web/produits');
        $body = json_decode($response->getBody(), true);
        
        $filters = Array();
        $filters['famille'] = Array();
        $filters['type'] = Array();
        $filters['annee'] = Array();
        $filters['prixVente'] = Array();

        //Filtres famille
        array_push($filters['famille'], 'Tout');
        foreach($body['produit'] as $product){
            if(!in_array($product['famille'], $filters['famille'])){
                array_push($filters['famille'], $product['famille']);
            }
        }

        //Filtres type
        array_push($filters['type'], 'Tout');
        foreach($body['produit'] as $product){
            if(!in_array($product['type'], $filters['type'])){
                array_push($filters['type'], $product['type']);
            }
        }
        
        //Filtres annee millesime
        array_push($filters['annee'], 'Tout');
        foreach($body['produit'] as $product){
            if(($product['annee']) != null){
                $decade = floor($product['annee'] / 10) * 10;
                
                if(!in_array($decade, $filters['annee'])){
                    array_push($filters['annee'], $decade);
                }
            }
        }

        //Filtres prix de vente
        foreach($body['produit'] as $product){
            if($product['prixVente'] != null){
                
                if(!in_array($product['prixVente'], $filters['prixVente'])){
                    array_push($filters['prixVente'], $product['prixVente']);
                }
            }
        }

        $max = max($filters['prixVente']);
        $min = min($filters['prixVente']);
        $filters['prixVente'] = Array();
        $filters['prixVente']['max'] = ceil($max);
        $filters['prixVente']['mid'] = ceil($max / 2);
        $filters['prixVente']['min'] = ceil($min);
        // array_push($filters['prix']['max'], ceil($max));
        // array_push($filters['prix']['mid'], ceil($max / 2));
        // array_push($filters['prix']['min'], ceil($min));

        /* $bracketCount = 5;
        $max = max($filters['prix']);
        $filters = Array();
        array_push($filters['prix'], $max);

        $max = round($max / 100) * 100;

        $filters['prix'] = Array();
                
        for($i=0; $i<$bracketCount; $i++){
            $price = ($max / $bracketCount) * ($i+1);
            array_push($filters['prix'], $price);
        } */
        
        return $filters;
    }

    public function getMaxPrice($products){
        $priceMax = 0;

        foreach($products as $product){
            $price = $product['prixVente'];
            if($price > $priceMax)
                $priceMax = $price;
        }

        return (int)$priceMax;
    }

}