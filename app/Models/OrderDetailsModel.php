<?php

namespace App\Models;

use CodeIgniter\Model;

class OrderDetailsModel extends Model
{
    private $client;

    function __construct()
    {
        /* $this->client = \Config\Services::curlrequest([
            'baseURI' => 'http://localhost:8080/apiv2/',
        ]); */
        $this->client = \Config\Services::curlrequest([
            'baseURI' => 'https://cube.fertille.fr/api/',
        ]);
    }

    /**
     * Enregistre un produit dans une commande
     * Retourne le code de la réponse http
     */
    public function createOrderDetails($orderId, $product){

        //On fixe la date de livraison à j+10 par défaut
        $date = date("Y-m-d", time());
        
        //On fixe le cout de livraison de l'article à 5% de son prix de vente. Ne peut être inferieur à 5€
        $deliveryCost = ceil(($product['prixVente'] / 100) * 10);
        $deliveryCost = (int)$deliveryCost < 5 ? 5 : $deliveryCost;
        
        $body = [
            "cout" => $deliveryCost,
            "description" => $product['nomArticle'],
            "idArticle" => $product['id'],
            "idCommande" => $orderId,
            "quantite" => $product['quantite'],
            "dateCommande" => $date
        ];
                
        $response = $this->client->request('POST', "detailscommandes", ['json'=>$body]);
        $codeStatus = $response->getStatusCode();

        return $codeStatus;
    }

    public function getById($id){
        $response = $this->client->request('GET', "detailscommandes/$id");
        $body = json_decode($response->getBody(), true);
        
        return $body['detailsCommande'];
    }
    
}