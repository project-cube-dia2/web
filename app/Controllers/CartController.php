<?php

namespace App\Controllers;

use App\Models\CustomersModel;
use App\Models\OrderDetailsModel;
use App\Models\OrdersModel;
use App\Models\ProductsModel;

class CartController extends BaseController
{
    private $session;
    private $productsModel;
    private $ordersModel;
    private $customersModel;
    private $orderDetailsModel;

    public function __construct(){
        $this->session = \Config\Services::session();

        // $this->session->set('panier', Array());
        if(!isset($_SESSION['panier'])){
            $this->session->set('panier', Array());
        }
        $this->productsModel = model(ProductsModel::class);
        $this->ordersModel = model(OrdersModel::class);
        $this->customersModel = model(CustomersModel::class);
        $this->orderDetailsModel = model(OrderDetailsModel::class);
    }
    
    public function index(){}

    public function view(){

        $panier = $this->session->get('panier');

        for($i=0; $i<count($panier); $i++){
            $product = $this->productsModel->getProductById($panier[$i]['id']);
            $panier[$i]['nomArticle'] = $product['nomArticle'];
            $panier[$i]['prixVente'] = $product['prixVente'];
            $panier[$i]['famille'] = $product['famille'];
            $panier[$i]['type'] = $product['type'];
            $panier[$i]['annee'] = $product['annee'];
        }

        $this->session->set('panier', $panier);
        
        $data = [
            'title' => "Negosud",
            'cart' => $this->session->get('panier'),
            'css' => [
                        'templates/header.css',
                        'components/banner.css',
                        'cart/grid.css',
                        'cart/product.css',
                        'components/footer.css',
                        'templates/footer.css',
            ],
            'js' => [
                'components/banner.js',
                'cart/product.js',
            ]
        ];
        
        return  view('templates/header', $data).
                view('components/banner').
                view('cart/grid').
                view('components/footer').
                view('cart/variables').
                view('templates/footer');
    }

    /**
     * Créer une nouvelle commande
     */
    public function buy(){

        /**
         * On récupère les champs du formulaire
         */
        $params = $this->request->getPost();

        /**
         * On enregistre le client en BDD ou on le récupère s'il est déjà enregistré
         */
        $id = $this->customersModel->existsInDB($params);
        if($id){
            $customer = $this->customersModel->getCustomersById($id);
        } else{
            $codeStatus = $this->customersModel->createCustomer($params);
            $id = $this->customersModel->existsInDB($params);
            $customer = $this->customersModel->getCustomersById($id);
        }
        
        /**
         * Création de la nouvelle commande
         */
        $newOrderId = $this->ordersModel->createOrder($customer, $this->session->get('panier'));
        
        /**
         * Enregistrement du contenu du panier en BDD. 
         * Dans la table details_commande, chaque entrée correspond à une entrée dans le panier.
         */
        $cart = $this->session->get('panier');
        foreach($cart as $product){
            $codeStatus = $this->orderDetailsModel->createOrderDetails($newOrderId, $product);
        }
        
        /**
         * On réinitialise le panier suite à l'achat
         */
        $this->session->set('panier', Array());
        
        $data = [
            'title' => "Negosud",
            'cart' => $this->session->get('panier'),
            'css' => [
                        'templates/header.css',
                        'components/banner.css',
                        'cart/redirectAfterPurchase.css',
                        'cart/product.css',
                        'components/footer.css',
                        'templates/footer.css',
            ],
            'js' => ['components/banner.js']
        ];
        
        return  view('templates/header', $data).
                view('components/banner').
                view('cart/redirectAfterPurchase').
                view('components/footer').
                view('cart/variables').
                view('templates/footer');
    }
    
    public function getCart(){
        return json_encode($this->session->get('panier'));
    }
    
    public function addProduct()
    {
        $params = $this->request->getPost();
        $idArticle = $params['id'];
        
        $panier = $this->session->get('panier');
        $idAlreadyExists = false;
        
        foreach($panier as $article){
            if(array_key_exists('id', $article)){
                if($article['id'] == $idArticle){
                    $idAlreadyExists = true;
                }
            }
        }

        if(!$idAlreadyExists || $panier ==  null){
            array_push($panier, $params);
        }

        if($idAlreadyExists){
            foreach($panier as $index => $article){
                if(array_key_exists('id', $article) && $article['id'] == $idArticle){
                    
                        if(array_key_exists('quantite', $article) && array_key_exists('quantite', $params)){
                            $quantiteArticle = (int)$article['quantite'];
                            $quantiteAjoutee = (int)$params['quantite'];
                            $article['quantite'] = (string)($quantiteArticle + $quantiteAjoutee);
                            $panier[$index] = $article;
                        }
                    
                }
            }
        }
        
        $this->session->set('panier', $panier);

        return json_encode($this->session->get('panier'));
        
    }

    public function removeProduct()
    {
        $params = $this->request->getPost();
        $idArticle = $params['id'];
        
        $panier = $this->session->get('panier');
        $idAlreadyExists = false;
        
        foreach($panier as $article){
            if(array_key_exists('id', $article)){
                if($article['id'] == $idArticle){
                    $idAlreadyExists = true;
                }
            }
        }

        if($idAlreadyExists){
            foreach($panier as $index => $article){
                if(array_key_exists('id', $article) && $article['id'] == $idArticle){
                    
                        if(array_key_exists('quantite', $article) && array_key_exists('quantite', $params)){
                            $quantiteArticle = (int)$article['quantite'];
                            $quantiteSupprimee = (int)$params['quantite'];
                            $article['quantite'] = (string)($quantiteArticle - $quantiteSupprimee);
                            $panier[$index] = $article;

                            if((int)$article['quantite'] <= 0){
                                unset($panier[$index]);
                                $this->session->set('panier', $panier);
                                return $this->view();
                            }
                        }
                        
                    }
                }
            }
            
        // var_dump($panier);
        $this->session->set('panier', $panier);

        return json_encode($this->session->get('panier'));
        
    }
}
