<?php

namespace App\Controllers;
use CodeIgniter\Exceptions\PageNotFoundException;

class PagesController extends BaseController
{
    public function index(){}

    public function view($page = 'home')
    {
        if (! is_file(APPPATH . 'Views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            throw new PageNotFoundException($page);
        }

        $data = [
            'title' => "Negosud",
            'css' => [
                        'templates/header.css',
                        'components/banner.css',
                        'components/footer.css',
                        'templates/footer.css',
            ]
        ];
        return view('templates/header', $data)
            . view('components/banner')
            . view('pages/' . $page)
            . view('components/footer')
            . view('templates/footer');
    }
}