<?php

namespace App\Controllers;
use App\Models\ProductsModel;

class CatalogController extends BaseController
{
    private $session;
    private $productsModel;

    public function __construct(){
        $this->session = \Config\Services::session();
        $this->productsModel = model(ProductsModel::class);
    }
    
    public function index()
    {
        $params = $this->request->getPost();

        $this->session->set('filterSettings', $params);
        $products = $this->productsModel->getProducts($params);
        // var_dump($this->session->get('panier'));

        //Si aucun filtres n'a encore été modifié par l'utilisateur, on leur attribu une valeur par défaut
        $filters = $this->productsModel->getProductFilters();
        $filters['userSettings'] = $this->session->get('filterSettings');
        if($filters['userSettings'] == null){
            $filters['userSettings'] = [
                'famille' => 'Tout',
                'type' => 'Tout',
                'annee' => 'Tout',
                'prixVente' => '50',
            ];
        }

        $data = [
            'title' => "Negosud",
            'products' => $products,
            'filters' => $filters,
            'css' => [
                        'templates/header.css',
                        'components/banner.css',
                        'components/grid.css',
                        'components/footer.css',
                        'templates/footer.css',
                        'components/product.css',
                        'components/filters.css',
            ],
            'js' => [
                        'components/grid.js',
                        'components/banner.js'
            ]
        ];
        
        return  view('templates/header', $data).
                view('components/banner').
                view('pages/cave').
                view('components/filters').
                view('components/grid').
                view('catalog/variables').
                view('components/footer').
                view('templates/footer');
    }

    public function view($filter=false, $value=false, $c=false){
        echo $filter;
        echo $value;
    }
}
